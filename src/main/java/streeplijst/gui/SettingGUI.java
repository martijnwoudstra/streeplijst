package streeplijst.gui;

import streeplijst.Setting;
import streeplijst.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

/**
 * Created by martijn on 29-2-16.
 */
public class SettingGUI  extends JFrame implements ActionListener{

    private final MainGUI mainGUI;
    private Setting setting;
    private JButton buttonAddUser;
    private JButton buttonRemoveUser;
    private JPanel panel;

    public SettingGUI(Setting setting, MainGUI mainGUI) {
        this.setting = setting;
        this.mainGUI = mainGUI;
        setLayout(new FlowLayout());

        panel = new JPanel();

        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        setMaximumSize(DimMax);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        //Default close and title
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        buttonAddUser = new JButton("Voeg gebruiker toe");
        buttonAddUser.addActionListener(this);
        buttonAddUser.setPreferredSize(new Dimension(300, 300));
        buttonAddUser.setVisible(true);

        buttonRemoveUser = new JButton("Verwijder gebruiker");
        buttonRemoveUser.addActionListener(this);
        buttonRemoveUser.setPreferredSize(new Dimension(300, 300));
        buttonRemoveUser.setVisible(true);

        panel.add(buttonAddUser);
        panel.add(buttonRemoveUser);
        add(panel);
        setVisible(true);
        panel.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        if (button.getText().contains("Voeg ")) {
            String s = (String) JOptionPane.showInputDialog(
                    this,
                    "Wie wil je toevoegen",
                    "Toevoegen",
                    JOptionPane.PLAIN_MESSAGE);
            setting.addUser(new User(s, 0, 0));
        }
        else if (button.getText().contains("Verwijder")){
            String s = (String) JOptionPane.showInputDialog(
                    this,
                    "Wie wil je verwijderen",
                    "Verwijderen",
                    JOptionPane.PLAIN_MESSAGE);
            User u = null;
            for(User user : setting.getUsers()){
                if (user.getName().equals(s))
                    u = user;
            }
            setting.removeUser(u);
        }
        setting.write();
        setting.start.rebuild(setting);
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
