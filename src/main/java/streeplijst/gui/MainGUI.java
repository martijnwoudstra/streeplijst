package streeplijst.gui;

import streeplijst.Setting;
import streeplijst.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;

/**
 * Created by martijn on 10-2-16.
 */
public class MainGUI extends JFrame implements ActionListener{

    private JPanel panel;
    private JPanel panel2;
    private JPanel panelSetting;
    private JButton settingButton;
    private JButton[] userDrunkenButton;
    private JButton[] userBoughtButton;
    private Setting setting;

    public MainGUI(Setting setting) {
        build(setting);
    }

    public void build(Setting setting) {
        this.setting = setting;
        userDrunkenButton = new JButton[setting.getUsers().size()];
        userBoughtButton = new JButton[setting.getUsers().size()];
        setLayout(new FlowLayout());

        //make grid layout, always 2 x a
        int a = (int) Math.ceil(setting.getUsers().size() / 3);
        panel = new JPanel(new GridLayout(3, a));
        panel2 = new JPanel(new GridLayout(3, a));
        panelSetting = new JPanel();
        add(panel);
        add(panel2);
        add(panelSetting);

        //Settings button
        settingButton = new JButton("Settings");
        settingButton.setPreferredSize(new Dimension(400, 200));
        settingButton.addActionListener(this);
        panelSetting.add(settingButton);

        //User buttons
        int i = 0;
        for(User user : setting.getUsers()){
            userDrunkenButton[i] = new JButton("Gedronken: " + user.getName() + " ("+ user.getSurplus()+")");
            userDrunkenButton[i].setPreferredSize(new Dimension(250, 250));
            userDrunkenButton[i].addActionListener(this);
            userDrunkenButton[i].setBackground(Color.GREEN);
            panel.add(userDrunkenButton[i]);
            i++;
        }

        //Bought buttons
        i = 0;
        for(User user : setting.getUsers()){
            userBoughtButton[i] = new JButton("Gekocht: " + user.getName() + " ("+ user.getSurplus()+")");
            userBoughtButton[i].setPreferredSize(new Dimension(250, 250));
            userBoughtButton[i].addActionListener(this);
            userBoughtButton[i].setBackground(Color.ORANGE);
            panel2.add(userBoughtButton[i]);
            i++;
        }


        //Set screen size
        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        setMaximumSize(DimMax);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        //Default close and title
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Streeplijst");

        //Visibility
        setVisible(true);
        panel.setVisible(true);
        panelSetting.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton)e.getSource();
        if (button.getText().contains("Settings")) {
            new SettingGUI(setting, this);
            return;
        }
        String name = button.getText().substring(button.getText().indexOf(':')+2, button.getText().indexOf('(') - 1);
        boolean drunken = button.getText().contains("Gedronken");
        User user = null;
        for (User u : setting.getUsers()){
            if (u.getName().equals(name))
                user = u;
        }
        if (user != null) {
            if (drunken) {
                Setting.LOGGER.log(Level.INFO, user.getName() + " drunk one beer");
                user.addBeerDrunken();
                setting.addDrunkenBeer();
                checkBeerDrunken();
            }
            else {
                String s = (String) JOptionPane.showInputDialog(
                        this,
                        "Hoeveel bier heb je gekocht?",
                        "Gekocht bier",
                        JOptionPane.PLAIN_MESSAGE);
                int i = Integer.parseInt(s);
                Setting.LOGGER.log(Level.INFO, user.getName() + " bought " + i + " beer");
                user.addBeerBought(i);
                setting.addBoughtBeer(i);
            }
        }
        for (int i = 0; i < userDrunkenButton.length; i++) {
            String t = userDrunkenButton[i].getText();
            String n = userDrunkenButton[i].getText().substring(t.indexOf(':')+2, t.indexOf('(') - 1);
            User us = null;
            for (User u : setting.getUsers()){
                if (u.getName().equals(n))
                    us = u;
            }
            if(us != null)
                userDrunkenButton[i].setText(t.substring(0, t.indexOf('(') + 1) + us.getSurplus()+")");
        }
        for (int i = 0; i < userBoughtButton.length; i++) {
            String t = userBoughtButton[i].getText();
            String n = userBoughtButton[i].getText().substring(t.indexOf(':')+2, t.indexOf('(') - 1);
            User us = null;
            for (User u : setting.getUsers()){
                if (u.getName().equals(n))
                    us = u;
            }
            if(us != null)
                userBoughtButton[i].setText(t.substring(0, t.indexOf('(') + 1) + us.getSurplus()+")");
        }
        setting.write();
    }

    private void checkBeerDrunken() {
        if(setting.getBeerLeft() < Setting.BEER_THRESHHOLD){
            String name = "";
            int i = 10000;
            for(User u : setting.getUsers()){
                if (u.getSurplus() < i) {
                    name = u.getName();
                    i = u.getSurplus();
                }
            }
            JOptionPane.showMessageDialog(this,
                    name + " should buy beer, go call him!");

        }
    }
}
