package streeplijst;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Settings class. Used for storing settings of the program, as well as users.
 */
public class Setting {

    /** Beer threshhold. How many beer are in stock before starts asking for rebuy */
    public static final int BEER_THRESHHOLD = 1;
    /** Logger for event log */
    public static final Logger LOGGER = Logger.getLogger( Setting.class.getName() );
    /** File handler for settings file */
    private FileHandler fh;
    /** Path to save file */
    private static final String PATH = "myoutputfile.txt";
    /** Separator between name, drunken beer and bought beer in output file. */
    public static final String SEPARATOR = "_";
    /** Separator before beer left in output file. */
    public static final String SEPERATOR2 = "?";
    /** List of users of the current <i>Streeplijst</i> */
    private List<User> users;
    /** Writer to write to save file*/
    private PrintWriter out;
    /** Reader to read from save file*/
    private BufferedReader in;
    /** Amount of beer left in stock */
    private int beerLeft;
    public final Start start;

    /**
     * Constructor for <Code>streeplijst.Setting</Code>
     * Creates {@link #users} and calls {@link #read()} to read from save file
     */
    public Setting (Start start){
        try {
            fh = new FileHandler("Userlog.log", true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fh.setFormatter(new SimpleFormatter());
        LOGGER.addHandler(fh);
        this.start = start;
        users = new ArrayList<User>();
        read();
    }

    /**
     * Method to add a user to the current <i>Streeplijst</i>
     * Users cannot contain the character {@link #SEPARATOR}
     * Also, a {@link User#getName()} can only occur once
     *
     * @param user      {@link User} to add to the list
     */
    public void addUser(User user){
        boolean canAdd = !user.getName().contains(SEPARATOR) || !user.getName().contains(SEPERATOR2);
        if (user.getName().contains("drunken"))
            canAdd = false;
        for(User u : users){
            if(u.getName().equals(user.getName()))
                canAdd = false;

        }
        if(canAdd) {
            users.add(user);
            LOGGER.log(Level.INFO, "Added user " + user.getName());
        } else {
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "User already exists!",
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }
        write();
    }

    public void addDrunkenBeer(){
        beerLeft--;
        write();
    }

    /**
     * Adds <i>Beer Bought</i> to the user
     *
     * @param packSize  How many have been bought (6 pack contains 6 pieces)
     */
    public void addBoughtBeer(int packSize){
        beerLeft += packSize;
        write();
    }

    /**
     * Gets the users in the current <i>Streeplijst</i>
     *
     * @return      {@link User}s in the current <i>Streeplijst</i>
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Writes to the save file.
     * Format:
     *      [name][SEPARATOR][beerBought][SEPARATOR][beerDrunken]\n
     */
    public void write(){
        try{
            out = new PrintWriter(PATH);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out.write(String.format("%s%d\n", SEPERATOR2, beerLeft));
        for(User user : users){
            out.write(String.format("%s%s%d%s%d\n", user.getName(), SEPARATOR, user.getBeerBought(), SEPARATOR, user.getBeerDrunken()));
        }
        out.close();
    }

    /**
     * Reads the data from save file, and saves the users with corresponding beers in {@link Setting#users}
     */
    public void read() {
        try {
            in = new BufferedReader(new FileReader(PATH));
            String line;
            String line2;
            if((line2 = in.readLine()) != null) {
                beerLeft = Integer.parseInt(line2.substring(1, line2.length()));
            }
            while((line = in.readLine()) != null && !(line.equals("")) && !(line.contains(SEPERATOR2))) {
                String[] linePieces = line.split(SEPARATOR);
                String name = linePieces[0];
                int bought = Integer.parseInt(linePieces[1]);
                int drunken = Integer.parseInt(linePieces[2]);
                User user = new User(name, drunken, bought);
                addUser(user);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        write();
    }

    /**
     * Returns the beer left in stock
     * @return beers left in stock
     */
    public int getBeerLeft() {
        return beerLeft;
    }

    /**
     * Remove user from user list
     * @param u     {@link User}
     */
    public void removeUser(User u) {
        LOGGER.log(Level.INFO, "Removed user " + u.getName() + ". His surplus was " + u.getSurplus());
        users.remove(u);
    }
}
