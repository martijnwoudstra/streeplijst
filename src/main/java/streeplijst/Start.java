package streeplijst;

import streeplijst.gui.MainGUI;

import java.awt.event.WindowEvent;

/**
 * Created by martijn on 9-2-16.
 */
public class Start {

    /** {@link Setting} object */
    Setting setting;
    /** MainGUI and UI object*/
   static MainGUI mainGUI;

    public static void main(String[] args) {
        Start start = new Start();
        start.start();
    }

    /**
     * Starts the Streeplijst.
     * Loads settings and adds users
     */
    public void start() {
        Setting setting = new Setting(this);
        mainGUI = new MainGUI(setting);
        for (User user : setting.getUsers())
            System.out.println(user.getName() + " " + user.getBeerBought() + " " + user.getBeerDrunken());
    }

    /**
     * Rebuilds main frame. Used for adding users
     * @param setting     {@link Setting} object
     */
    public void rebuild(Setting setting){
        mainGUI.dispatchEvent(new WindowEvent(mainGUI, WindowEvent.WINDOW_CLOSING));
        mainGUI = new MainGUI(setting);
    }
}
