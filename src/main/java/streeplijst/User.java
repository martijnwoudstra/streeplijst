package streeplijst;

/**
 * Created by martijn on 9-2-16.
 */
public class User {

    /** Amount of beer drunken*/
    private int beerDrunken;
    /** Amount of beer bought*/
    private int beerBought;
    /** Unique name of the user*/
    private String name;

    /**
     * Constructor for <code>streeplijst.User</code>
     * @param name          Unique name of the <code>user</code>, cannot contain {@link Setting#SEPARATOR}
     * @param beerDrunken   Beer that has been drunken by <code>user</code>
     * @param beerBought    Beer that has been bought by <code>user</code>
     */
    public User(String name, int beerDrunken, int beerBought){
        this.name = name;
        this.beerDrunken = beerDrunken;
        this.beerBought = beerBought;
    }

    /**
     * Add beer bought
     *
     * @param amount    Amount bought
     */
    public void addBeerBought(int amount){
        beerBought += amount;
    }

    /**
     * Adds one beer drunken
     */
    public void addBeerDrunken(){
        beerDrunken++;
    }

    /**
     * Gets unique name of the user
     *
     * @return      Unique name of the user
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the beer drunken by <code>user</code>
     *
     * @return      Amount beer drunken
     */
    public int getBeerDrunken() {
        return beerDrunken;
    }

    /**
     * Gets the beer bought by <code>user</code>
     *
     * @return      Amount beer bought
     */
    public int getBeerBought() {
        return beerBought;
    }

    /**
     * Returns the surplus
     * Surplus = beerBought - beerDrunken;
     */
    public int getSurplus(){
        return beerBought - beerDrunken;
    }
}
